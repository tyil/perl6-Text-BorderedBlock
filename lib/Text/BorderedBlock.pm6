#! /usr/bin/env false

use v6.c;

use Terminal::ANSIColor;

#| Create a border around a block of text. This will only have the intended
#| effect if you're using fixed width text, such as in a terminal.
class Text::BorderedBlock
{
	#| The characters which will be used to render a box around the text.
	has Str %.box-characters =
		outer-top-left     => "┏",
		outer-top-right    => "┓",
		outer-bottom-left  => "┗",
		outer-bottom-right => "┛",
		outer-horizontal   => "━",
		outer-vertical     => "┃",
		seperator-left     => "┠",
		seperator-right    => "┨",
		seperator          => "─",
	;

	#| The block's main body content.
	has Str $.content = "";

	#| An optional header text.
	has Str $.header = "";

	#| An optional footer text.
	has Str $.footer = "";

	#| The minimum width of the box. This can be used to make multiple boxes
	#| look more uniform.
	has Int $.minimum-width = 66;

	#| Render a block of text with a border around it.
	multi method render (
		--> Str
	) {
		self.render(
			$!content,
			header => $!header,
			footer => $!footer,
			minimum-width => $!minimum-width,
		);
	}

	#| Render a block of text with a border around it.
	multi method render (
		#| The text to show inside the block.
		Str:D $content is copy,

		#| Optional header text. Will overwrite the header text set on the
		#| BorderedBlock instance.
		:$header = $!header,

		#| Optional footer text. Will overwrite the footer text set on the
		#| BorderedBlock instance.
		:$footer = $!footer,

		#| The minimum width of the block.
		:$minimum-width = $!minimum-width,
		--> Str
	) {
		# Trim content
		$content .= trim;

		# Calculate the maximum width of the content
		my Int $longest-string = 0;

		for $content.lines { $longest-string = colorstrip($_).chars if $longest-string < colorstrip($_).chars; }
		for $header.lines  { $longest-string = colorstrip($_).chars if $longest-string < colorstrip($_).chars; }
		for $footer.lines  { $longest-string = colorstrip($_).chars if $longest-string < colorstrip($_).chars; }

		$longest-string = $minimum-width if $longest-string < $minimum-width;

		# Create the top bar
		my Str $block = %!box-characters<outer-top-left>;
		$block ~= %!box-characters<outer-horizontal> x $longest-string;
		$block ~= %!box-characters<outer-top-right>;
		$block ~= "\n";

		# Add a header, if needed
		if ($header ne "") {
			for $header.lines {
				$block ~= self!wrap-line($_, $longest-string);
			}

			$block ~= %!box-characters<seperator-left>;
			$block ~= %!box-characters<seperator> x $longest-string;
			$block ~= %!box-characters<seperator-right>;
			$block ~= "\n";
		}

		# Add the main content
		for $content.lines {
			$block ~= self!wrap-line($_, $longest-string);
		}

		# Add a footer, if needed
		if ($footer ne "") {
			$block ~= %!box-characters<seperator-left>;
			$block ~= %!box-characters<seperator> x $longest-string;
			$block ~= %!box-characters<seperator-right>;
			$block ~= "\n";

			for $footer.lines {
				$block ~= self!wrap-line($_, $longest-string);
			}
		}

		# Create the bottom bar
		$block ~= %!box-characters<outer-bottom-left>;
		$block ~= %!box-characters<outer-horizontal> x $longest-string;
		$block ~= %!box-characters<outer-bottom-right>;
	}

	method !wrap-line (
		Str:D $line,
		Int:D $length,
		--> Str
	) {
		%!box-characters<outer-vertical>
			~ $line ~ " " x ($length - colorstrip($line).chars)
			~ %!box-characters<outer-vertical> ~ "\n";
	}
}

=begin pod

=NAME    Text::BorderedBlock
=AUTHOR  Patrick Spek
=VERSION 0.1.0
=LICENSE AGPL-3.0

=head1 Description

Create a fixed-width block around some text. This has the best effects if the
text itself is also fixed-width. Optionally, the block can also be given a
header block and a footer block. This allows you to add some context to the
block's content.

=head1 Examples

=head2 Default usage

=begin input
use Text::BorderedBlock;

my Text::BorderedBlock $block .= new;

say $block.render("Some content to put a border around")
=end input

=begin code
┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
as code
=end code

=begin output
┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
as output
=end output

=head2 A block with a header

=begin input
use Text::BorderedBlock;

my Text::BorderedBlock $block .= new;

say $block.render(
	"Imagine some source code here",
	header => "lib/Some/Sample.pm",
);
=end input

=begin output
n-nani?
=end output

=end pod

# vim: ft=perl6 noet

#! /usr/bin/env perl6

use v6.c;

use Text::BorderedBlock;
use Test;

plan 3;

my Text::BorderedBlock $block .= new;

subtest "Empty block" => {
	plan 1;

	is
		$block.render(""),
		slurp("t/files/empty.txt").trim,
		"Empty block";
}

subtest "Single lines" => {
	plan 4;

	my Str $text = "Lorem ipsum dolor sit amet...";

	is
		$block.render($text),
		slurp("t/files/simple.txt").trim,
		"Simple block";

	is
		$block.render($text, header => $text),
		slurp("t/files/header.txt").trim,
		"Block with header";

	is $block.render($text, footer => $text),
		slurp("t/files/footer.txt").trim,
		"Block with footer";

	is $block.render($text, header => $text, footer => $text),
		slurp("t/files/full.txt").trim,
		"Block with header and footer";
}

subtest "Multiple lines" => {
	plan 4;

	my Str $text = q:to/END/;
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent
        scelerisque, mi in rutrum molestie, lacus sem pellentesque justo, faucibus
        hendrerit purus eros quis tellus. Proin id pretium leo, quis venenatis
        ligula. Aliquam vel scelerisque dolor. Nam tristique sodales posuere. Sed
        rhoncus erat leo, vel volutpat odio tincidunt non. Praesent pharetra felis
        et semper condimentum. Curabitur a lorem eget purus facilisis mollis. Sed
        eu auctor metus. Vivamus id scelerisque libero, feugiat interdum neque.
        Aliquam vitae sem sit amet mi porta interdum.
        END


	is
		$block.render($text),
		slurp("t/files/multi-simple.txt").trim,
		"Simple block.";

	is
		$block.render($text, header => $text),
		slurp("t/files/multi-header.txt").trim,
		"Block with header";

	is $block.render($text, footer => $text),
		slurp("t/files/multi-footer.txt").trim,
		"Block with footer";

	is $block.render($text, header => $text, footer => $text),
		slurp("t/files/multi-full.txt").trim,
		"Block with header and footer";


}

# vim: ft=perl6 noet
